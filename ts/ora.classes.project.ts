import * as plugins from './ora.plugins';
import { Ora } from './ora.classes.ora';
import { OraOrganization } from './ora.classes.organization';
import { OraList } from './ora.classes.list';
import { OraMilestone } from './ora.classes.milestone';

export interface IOraProject {
  archived: boolean;
  comment_all: number;
  commits_visibility: number;
  created_at: string;
  default_view: boolean;
  description: string;
  id: number;
  invite_all: boolean;
  item_name: 'task';
  items_name: 'tasks';
  organization_id: number;
  owner: number;
  picture: string;
  prefix: string;
  project_color: string;
  project_type: number;
  seen: boolean;
  starred: number;
  tasks: number;
  title: string;
  updated_at: string;
  view_title: 'Backlog';
  web: string;
}

export class OraProject implements IOraProject {
  // ======
  // STATIC
  // ======
  public static async getAllProjectsForOrganization(oraOrganizationRef: OraOrganization) {
    const response = await oraOrganizationRef.oraRef.request('/projects', 'GET');
    const projects: OraProject[] = [];
    for (const projectData of response.data) {
      const oraProject = new OraProject(oraOrganizationRef, projectData);
      if (oraProject.organization_id === oraOrganizationRef.id) {
        projects.push(oraProject);
      }
    }
    return projects;
  }

  // ========
  // INSTANCE
  // ========
  public archived: boolean;
  public comment_all: number;
  public commits_visibility: number;
  public created_at: string;
  public default_view: boolean;
  public description: string;
  public id: number;
  public invite_all: boolean;
  public item_name: 'task';
  public items_name: 'tasks';
  public organization_id: number;
  public owner: number;
  public picture: string;
  public prefix: string;
  public project_color: string;
  public project_type: number;
  public seen: boolean;
  public starred: number;
  public tasks: number;
  public title: string;
  public updated_at: string;
  public view_title: 'Backlog';
  public web: string;

  public oraOrganizationRef: OraOrganization;
  constructor(oraOrganiazionRefArg: OraOrganization, creationObjectArg: IOraProject) {
    this.oraOrganizationRef = oraOrganiazionRefArg;
    Object.assign(this, creationObjectArg);
  }

  public async getLists(): Promise<OraList[]> {
    return OraList.getAllLists(this);
  }

  public async getMileStones(): Promise<OraMilestone[]> {
    return OraMilestone.getAllMilestonesForProject(this);
  }
}
