import * as plugins from './ora.plugins';
import { OraProject } from './ora.classes.project';
import { OraTask } from './ora.classes.task';

/**
 * the interface for an ora list
 */
export interface IOraList {
  actions: number;
  archived: false;
  color: string;
  id: number;
  list_type: number;
  position: number;
  project_id: number;
  public: boolean;
  title: string;
  updated_at: string;
  view_id: number;
}

export class OraList implements IOraList {
  // ======
  // STATIC
  // ======
  public static async getAllLists(oraProjectRef: OraProject) {
    const response = await oraProjectRef.oraOrganizationRef.oraRef.request(
      `/projects/${oraProjectRef.id}/lists`,
      'GET'
    );
    const oraLists: OraList[] = [];
    for (const dataObject of response.data) {
      oraLists.push(new OraList(oraProjectRef, dataObject));
    }
    return oraLists;
  }

  // ========
  // INSTANCE
  // ========
  public actions: number;
  public archived: false;
  public color: string;
  public id: number;
  public list_type: number;
  public position: number;
  public project_id: number;
  public public: boolean;
  public title: string;
  public updated_at: string;
  public view_id: number;

  public oraProjectObjectRef: OraProject;

  constructor(oraProjectRefArg: OraProject, creationObjectArg) {
    this.oraProjectObjectRef = oraProjectRefArg;
    Object.assign(this, creationObjectArg);
  }

  public async getTasks() {
    return OraTask.getAllOraTasks(this);
  }
}
